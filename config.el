;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets. It is optional.
(setq user-full-name "Christian Kruse"
      user-mail-address "christian@kruse.cool")

;; Doom exposes five (optional) variables for controlling fonts in Doom:
;;
;; - `doom-font' -- the primary font to use
;; - `doom-variable-pitch-font' -- a non-monospace font (where applicable)
;; - `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;; - `doom-unicode-font' -- for unicode glyphs
;; - `doom-serif-font' -- for the `fixed-pitch-serif' face
;;
;; See 'C-h v doom-font' for documentation and more examples of what they
;; accept. For example:
;;
;;(setq doom-font (font-spec :family "Fira Code" :size 12 :weight 'semi-light)
;;      doom-variable-pitch-font (font-spec :family "Fira Sans" :size 13))
;;
;; If you or Emacs can't find your font, use 'M-x describe-font' to look them
;; up, `M-x eval-region' to execute elisp code, and 'M-x doom/reload-font' to
;; refresh your font settings. If Emacs still can't find your font, it likely
;; wasn't installed correctly. Font issues are rarely Doom issues!

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-font (font-spec :family "Source Code Pro" :size 13 :weight 'regular)
      doom-variable-pitch-font (if IS-LINUX
                                   (font-spec :family "Source Sans 3" :size 14)
                                 (font-spec :family "Source Sans Pro" :size 12))
      doom-theme 'doom-one
      org-directory "~/org/"
      display-line-numbers-type t
      default-directory "~/"
      indent-tabs-mode nil
      window-resize-pixelwise t
      frame-resize-pixelwise t
      +tree-sitter-hl-enabled-modes t
      fancy-splash-image (concat doom-user-dir "vagabond.png")
      truncate-string-ellipsis "…")

(setq-default show-trailing-whitespace t
              require-final-newline t)

(when (and (fboundp 'menu-bar-mode)
           (not IS-MAC))
  (menu-bar-mode -1))
(when (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(when (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))

(setf dired-kill-when-opening-new-dired-buffer t)

(custom-set-faces!
  '(aw-leading-char-face
    :foreground "white" :background "red"
    :weight bold :height 2.0 :box (:line-width 10 :color "red")))

(map!
 "<s-up>" #'beginning-of-buffer
 "<s-down>" #'end-of-buffer

 "s-d" #'mc/mark-next-like-this
 "s-D" #'mc/mark-all-like-this
 "M-s-d" #'mc/mark-next-like-this
 "C-s-d" #'mc/edit-lines
 "C-c c C-c" (cmd! (save-excursion (comment-line 1)))

 "C-x C-j" #'dired-jump
 "s-+" #'er/expand-region
 "s-=" #'er/expand-region
 "s-u" #'revert-buffer

 (:after smartparens
  :map smartparens-mode-map
  "C-<left>" nil
  "C-<right>" nil
  "M-<left>" nil
  "M-<right>" nil)

 ;; (:after js2-mode
 ;;  (:map js2-mode-map
 ;;   "M-." nil))

 (:when IS-LINUX
  ;; "H-s-<up>" #'beginning-of-buffer
  ;; "H-s-<down>" #'end-of-buffer
  "s-<left>" #'doom/backward-to-bol-or-indent
  "s-<right>" #'doom/forward-to-last-non-comment-or-eol
  "s-c" #'kill-ring-save
  "s-v" #'yank
  "s-z" #'undo-fu-only-undo
  "s-Z" #'undo-fu-only-redo))
;; "H-s-u" #'revert-buffer))


(map!
 :leader
 :prefix "u"
 "fi" (lambda ()
        (interactive)
        (find-file "~/org/inbox.org"))
 "fp" (lambda ()
        (interactive)
        (find-file "~/org/passwords.org.gpg")))

(unbind-key "M--")
(unbind-key "s--")

(after! org
  (setq! org-startup-indented nil
         org-startup-folded 'content
         org-agenda-files (file-expand-wildcards "~/org/*.org")
         org-capture-templates '(("t" "todo" entry (file+headline "~/org/inbox.org" "Inbox")
                                  "* TODO %?\n%U\n%a\n")
                                 ("n" "note" entry (file+headline "~/org/notes.org" "Notizen")
                                  "* %?\n%U\n")
                                 ("w" "blog entry" entry (file+headline "~/org/inbox.org" "Inbox")
                                  "* TODO %? :blog:\n%U\n")
                                 ("m" "Meeting" entry (file+headline "~/org/inbox.org" "Inbox")
                                  "* MEETING with %? :MEETING:\n%U")
                                 ("c" "Phone call" entry (file+headline "~/org/inbox.org" "Inbox")
                                  "* PHONE %? :PHONE:\n%U")
                                 ("P" "password" entry (file "~/org/passwords.gpg")
                                  "* %^{Title}\n  %^{URL}p %^{USERNAME}p %^{PASSWORD}p"))))


;; (after! js2-mode
;;   (setq js2-basic-offset 2))

(after! css-mode
  (setq css-indent-offset 2))

(after! javascript-mode
  (setq javascript-indentation 2
        js-indent-level 2))

(after! typescript
  (setq typescript-indent-level 2))

(after! json-mode
  (setq javascript-indentation 2
        js-indent-level 2))

(after! web-mode
  (setq web-mode-engines-alist '(("django" . "\\.html\\.tera\\'")))
  (add-hook! web-mode
    (setq web-mode-markup-indent-offset 2
          web-mode-css-indent-offset 2
          web-mode-code-indent-offset 2)))

(add-to-list 'auto-mode-alist (cons (rx ".html.tera" string-end) #'web-mode))
(add-to-list 'auto-mode-alist (cons (rx ".js" string-end) #'javascript-mode))

(use-package! uniquify
  :config
  (setq uniquify-buffer-name-style 'forward))

(after! magit
  (push (cons [unpushed status] 'show) magit-section-initial-visibility-alist)
  (push (cons [stashes status] 'show) magit-section-initial-visibility-alist))

(after! lsp-mode
  (push "[/\\\\]_build\\'" lsp-file-watch-ignored-directories)
  (push "[/\\\\]deps\\'" lsp-file-watch-ignored-directories)
  (push "[/\\\\]cover\\'" lsp-file-watch-ignored-directories)
  (push "[/\\\\]priv\\'" lsp-file-watch-ignored-directories)
  (push "[/\\\\]\\.deliver\\'" lsp-file-watch-ignored-directories)
  (push "[/\\\\]\\.elixir_ls\\'" lsp-file-watch-ignored-directories)
  (push "[/\\\\]\\.build\\'" lsp-file-watch-ignored-directories)
  (push "[/\\\\]node_modules\\'" lsp-file-watch-ignored-directories)
  (setq lsp-enable-folding nil))

(add-hook! elixir-mode
  (add-hook 'before-save-hook 'lsp-format-buffer nil t)
  (pushnew! flycheck-disabled-checkers 'elixir-credo))

(add-hook! rustic-mode
  (add-hook 'before-save-hook 'lsp-format-buffer nil t))

(after! rustic
  (setq rustic-lsp-server 'rust-analyzer
        lsp-rust-analyzer-server-display-inlay-hints t))

(add-hook! js-mode 'lsp!)

(use-package! tsx-mode
  :mode (("\\.tsx\\'" . tsx-mode)
         ("\\.ts\\'" . tsx-mode))
  :hook (tsx-mode . lsp!)
  :hook (tsx-mode . rainbow-delimiters-mode)
  :hook (tsx-mode . +javascript-add-npm-path-h)
  :custom (tsx-mode-tsx-auto-tags  t)
  :defer t
  :init
  (after! flycheck
    (flycheck-add-mode 'javascript-eslint 'tsx-mode))

  (add-hook! 'tsx-mode-hook
    (defun ck/tsx-setup ()
      (flycheck-select-checker 'javascript-eslint)
      (flycheck-add-next-checker 'javascript-eslint 'lsp)
      (pushnew! flycheck-disabled-checkers
                'javascript-jshint
                'tsx-tide
                'jsx-tide)))

  (set-electric! 'tsx-mode
    :chars '(?\} ?\))
    :words '("||" "&&")))

(use-package! apheleia
  :hook ((tsx-mode . apheleia-mode)
         (typescript-mode . apheleia-mode)
         (js-mode . apheleia-mode)
         (json-mode . apheleia-mode)
         (css-mode . apheleia-mode)
         (scss-mode . apheleia-mode))
  :defer t
  :config
  (push '(tsx-mode . prettier) apheleia-mode-alist)
  (push '(scss-mode . prettier) apheleia-mode-alist)
  (push '(css-mode . prettier) apheleia-mode-alist))


(use-package! goggles
  :defer t
  :hook ((prog-mode text-mode) . goggles-mode))

(use-package! deadgrep
  :defer t)

(use-package! fussy
  :config
  (push 'fussy completion-styles)
  (setq
   completion-category-defaults nil
   completion-category-overrides nil))

(after! highlight-indent-guides
  (setq highlight-indent-guides-method 'fill
        highlight-indent-guides-auto-odd-face-perc 3
        highlight-indent-guides-auto-even-face-perc 1.5))

(use-package! copilot
  :hook (prog-mode . copilot-mode)
  :bind (("C-TAB" . 'copilot-accept-completion-by-word)
         ("C-<tab>" . 'copilot-accept-completion-by-word)
         :map copilot-completion-map
         ("<tab>" . 'copilot-accept-completion)
         ("TAB" . 'copilot-accept-completion))
  :config
  (setq copilot-node-executable "/opt/homebrew/opt/node@16/bin/node"))
