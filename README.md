# doom.d

This is my personal [Doom Emacs](https://github.com/hlissner/doom-emacs/) configuration. It focuses on web development in Elixir and React, but has some Rust integration, too.
